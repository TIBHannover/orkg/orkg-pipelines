# ORKG Pipelines

NLP pipelines to extract aligned triples from snippets of text

## Running Instructions

You just need to do `docker-compose up -d`

To make sure that everything is up to date, just pull the latest images before you run the service using `docker-compose pull`

## Other Remarks

In the docker-compose file under the `plumber` service you will find the env variables to run the external services, you can change them in accourdance with your system if needed.
